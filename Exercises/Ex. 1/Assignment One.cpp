#include <cstdio>

/**
 * A simple struct containing an array
 */
struct arrStruct {
  int arr[5];
};

/**
 * Increment values in array by one
 * @param arr array to mutate
 */
void incrementReference(int arr[5]) {
  for (int i = 0; i < 5; ++i) {
    arr[i]++;
  }
}

/**
 * Increment values in array by one accessed through struct
 * @param structArr struct to mutate
 */
void incrementValue(arrStruct structArr) {
  for (int i = 0; i < 5; ++i) {
    structArr.arr[i]++;
  }
}

/**
 * @param arr array to print
 */
void printArrayReference(int arr[5]) {
  for (int i = 0; i < 5; ++i) {
    printf("%i ", arr[i]);
  }
  printf("\n");
}

/**
 * Print array from struct
 * @param structArr struct to print
 */
void printArrayValue(arrStruct structArr) {
  for (int i = 0; i < 5; ++i) {
    printf("%i ", structArr.arr[i]);
  }
  printf("\n");
}

int main() {
  int arrOne[5] = {0,1,2,3,4};
  printf("ARRAY BY REF BEFORE\n");
  printArrayReference(arrOne);
  incrementReference(arrOne);
  printf("ARRAY BY REF AFTER\n");
  printArrayReference(arrOne);

  arrStruct arrTwoStruct;
  // Initialize values in struct data member.
  arrTwoStruct.arr[0] = 0;
  arrTwoStruct.arr[1] = 1;
  arrTwoStruct.arr[2] = 2;
  arrTwoStruct.arr[3] = 3;
  arrTwoStruct.arr[4] = 4;
  printf("ARRAY BY VALUE BEFORE\n");
  printArrayValue(arrTwoStruct);
  printf("ARRAY BY VALUE AFTER\n");
  incrementValue(arrTwoStruct);
  printArrayValue(arrTwoStruct);

  return 0;
}
