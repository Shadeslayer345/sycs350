(defparameter *player* 1)
(defparameter *computer* 10)
(defvar *triplets* '((1 2 3) (4 5 6) (7 8 9) (1 4 7) (2 5 8) (3 6 9) (1 5 9) (3 5 7)))
(defvar *computer-score* 0)
(defvar *player-score* 0)

#|| Board Logic  ||#
(defun make-board() (list 'board 0 0 0 0 0 0 0 0 0))

(defun convert-to-letter (x)
  (cond
    ((equal x 1)
      "O")
    ((equal x 10)
      "X")
    (t " ")))

(defun print-row (x y z)
  (format t "~& ~A | ~A | ~A"
    (convert-to-letter x)
    (convert-to-letter y)
    (convert-to-letter z)))

(defun print-board (board)
  (format t "~%")
  (print-row
    (nth 1 board) (nth 2 board) (nth 3 board))
  (format t "~& -----------")
  (print-row
    (nth 4 board) (nth 5 board) (nth 6 board))
  (format t "~& -----------")
  (print-row
    (nth 7 board) (nth 8 board) (nth 9 board))
  (format t "~%~%"))

#|| Game Logic (verifying input, determining winner, etc) ||#

(defun read-move (board)
  (format t "~&Your move: ")
  (let ((pos (read)))
    (cond
      ((not (and (integerp pos) (<= 1 pos 9)))
        (format t "~&Invalid input.")
        (read-move board))
      ((not (zerop (nth pos board)))
        (format t "~&That space is already occupied")
        (read-move board))
      (t pos))))

(defun sum-triplet (board triplet)
  (+
    (nth (first triplet) board)
    (nth (second triplet) board)
    (nth (third triplet) board)))

(defun compute-sums (board)
  (mapcar #'(lambda (triplet)
              (sum-triplet board triplet))
    *triplets*))

(defun check-for-scratch (board)
  (not (member 0 board)))

(defun check-for-winner (board)
  (let ((sums (compute-sums board)))
    (or
      (member (* 3 *player*) sums)
      (member (* 3 *computer*) sums))))

(defun make-move (player pos board)
  (setf (nth pos board) player)
  board)

#|| Player Logic ||#

(defun player-move (board)
  (let* ((pos (read-move board))
         (new-board (make-move *player* pos board)))
    (print-board new-board)
    (cond
      ((check-for-winner new-board)
        (format t "~&You win!")
        (setf *player-score* (+ 5 *player-score*)))
      ((check-for-scratch new-board)
        (format t "~&Tie Game."))
      (t (computer-move new-board)))))

#|| AI Logic ||#

(defun pick-random-empty-position (board)
  (let ((pos (+ 1 (random 9))))
    (if (zerop (nth pos board))
      pos
      (pick-random-empty-position board))))

(defun random-move (board)
  (list
    (pick-random-empty-position board)
    "random move"))

(defun find-empty-position (board squares)
  (find-if #'(lambda (pos)
               (zerop (nth pos board)))
    squares))

(defun win-or-block (board pos-sum)
  (let ((triplet (find-if #'(lambda (trip)
                              (equal (sum-triplet board trip) pos-sum))
                   *triplets*)))
    (when triplet
      (find-empty-position board triplet))))

(defun make-three-in-a-row (board)
  (let ((pos (win-or-block board (* 2 *computer*))))
    (and pos (list pos "make three in a row"))))

(defun block-player-win (board)
  (let ((pos (win-or-block board (* 2 *player*))))
    (and pos (list pos "block player"))))

(defun choose-best-move (board)
  (or
    (make-three-in-a-row board)
    (block-player-win board)
    (random-move board)))

(defun computer-move (board)
  (let* ((best-move (choose-best-move board))
         (pos (first best-move))
         (strategy (second best-move))
         (new-board (make-move *computer* pos board)))
    (format t "~&My move: ~S" pos)
    (format t "~&My strategy: ~A~%" strategy)
    (print-board new-board)
    (cond
      ((check-for-winner new-board)
        (format t "~&I win!")
        (setf *computer-score* (+ 5 *computer-score*)))
      ((check-for-scratch new-board)
        (format t "~&Tie game."))
      (t (player-move new-board)))))

#|| Play options ||#

(defun one-play ()
  (if (y-or-n-p "Would you like to go first? ")
    (player-move (make-board))
    (computer-move (make-board))))

(defun multi-play ()
  (loop
    (if (y-or-n-p "Would you like to play Tic-Tac-Toe? ")
      (one-play)
      (return))
    (if (not (y-or-n-p "Would you like to play again? "))
      (return)))
  (format t "Computer Score ~S~%" *computer-score*)
  (format t "Player Score ~S~%" *player-score*))

#||
[1]> (load "one.lsp")
;; Loading file one.lsp ...
;; Loaded file one.lsp
T
[2]> (multi-play)
Would you like to play Tic-Tac-Toe?  (y/n) y
Would you like to go first?  (y/n) n
My move: 9
My strategy: random move

   |   |
 -----------
   |   |
 -----------
   |   | X

Your move: 6

   |   |
 -----------
   |   | O
 -----------
   |   | X

My move: 5
My strategy: random move

   |   |
 -----------
   | X | O
 -----------
   |   | X

Your move: 1

 O |   |
 -----------
   | X | O
 -----------
   |   | X

My move: 2
My strategy: random move

 O | X |
 -----------
   | X | O
 -----------
   |   | X

Your move: 8

 O | X |
 -----------
   | X | O
 -----------
   | O | X

My move: 7
My strategy: random move

 O | X |
 -----------
   | X | O
 -----------
 X | O | X

Your move: 3

 O | X | O
 -----------
   | X | O
 -----------
 X | O | X

My move: 4
My strategy: random move

 O | X | O
 -----------
 X | X | O
 -----------
 X | O | X

Tie game.
Would you like to play again?  (y/n) y
Would you like to play Tic-Tac-Toe?  (y/n) y
Would you like to go first?  (y/n) y
Your move: 9

   |   |
 -----------
   |   |
 -----------
   |   | O

My move: 1
My strategy: random move

 X |   |
 -----------
   |   |
 -----------
   |   | O

Your move: 5

 X |   |
 -----------
   | O |
 -----------
   |   | O

My move: 2
My strategy: random move

 X | X |
 -----------
   | O |
 -----------
   |   | O

Your move: 3

 X | X | O
 -----------
   | O |
 -----------
   |   | O

My move: 6
My strategy: block player

 X | X | O
 -----------
   | O | X
 -----------
   |   | O

Your move: 8

 X | X | O
 -----------
   | O | X
 -----------
   | O | O

My move: 7
My strategy: block player

 X | X | O
 -----------
   | O | X
 -----------
 X | O | O

Your move: 4

 X | X | O
 -----------
 O | O | X
 -----------
 X | O | O

TIe Game.
Would you like to play again?  (y/n) n
Computer Score 0
Player Score 0
||#
