; lisp program for a simple inventory system.

(defvar *exotic-cars* '(car1 car2 car3 car4 car5 car6 car7 car8 car9 car10))

(defun fetchinput ()
  (let ((count 0))
    (with-open-file (mystream "cars.dat")
      (loop
        (setf (get (nth count *exotic-cars*) 'make) (read mystream))
        (setf (get (nth count *exotic-cars*) 'model) (read mystream))
        (setf (get (nth count *exotic-cars*) 'cost) (read mystream))
        (setf count (+ count 1))
        (when (> count 9) (return))))))

(defun printlist ()
  (format t "~2%~1,15T ~A ~2,15T ~A ~3,15T ~A ~% ~A"
    'Make 'Model 'Cost '-------------------------------------)
  (let ((count 0))
    (loop
      (format t "~%~1,15T ~A ~2,15T ~A ~3,15T $~D.00"
          (get (nth count *exotic-cars*) 'make)
          (get (nth count *exotic-cars*) 'model)
          (get (nth count *exotic-cars*) 'cost))
      (setf count (+ count 1))
      (when (> count 9) (return)))))

(defun findmake ()
  (terpri)
  (terpri)
  (princ"Enter car model to search for: ")
  (let* ((search (read)) (count 0))
    (loop
      (if (string= (get (nth count *exotic-cars*) 'make) search)
        (format t"~2%~1,15T ~A ~2,15T ~A ~3,15T ~A ~% ~A~%~1,15T ~A ~2,15T ~A ~3,15T $~D.00"
          'Make 'Model 'Cost '-------------------------------------
          (get (nth count *exotic-cars*) 'make)
          (get (nth count *exotic-cars*) 'model)
          (get (nth count *exotic-cars*) 'cost)))
      (setf count (+ count 1))
      (when (> count 9) (return)))))

(defun swap (from to)
  (let ((tmpmake (get (nth to *exotic-cars*) 'make))
        (tmpmodel (get (nth to *exotic-cars*) 'model))
        (tmpcost (get (nth to *exotic-cars*) 'cost)))
    (setf (get (nth to *exotic-cars*) 'make) (get (nth from *exotic-cars*) 'make))
    (setf (get (nth to *exotic-cars*) 'model) (get (nth from *exotic-cars*) 'model))
    (setf (get (nth to *exotic-cars*) 'cost) (get (nth from *exotic-cars*) 'cost))
    (setf (get (nth from *exotic-cars*) 'make) tmpmake)
    (setf (get (nth from *exotic-cars*) 'model) tmpmodel)
    (setf (get (nth from *exotic-cars*) 'cost) tmpcost)))

(defun mysort ()
  (let ((unsorted 1))
    (loop
      (let ((current 9))
        (loop
          (if (< (get (nth current *exotic-cars*) 'cost) (get (nth (- current 1) *exotic-cars*) 'cost))
            (swap current (- current 1)))
          (setf current (- current 1))
          (when (< current unsorted) (return))))
      (setf unsorted (+ unsorted 1))
      (when (> unsorted 9) (return)))
    (printlist)))

(defun dostuff ()
  (fetchinput)
  (printlist)
  (findmake)
  (terpri)
  (mysort))
#||
Break 4 [13]> (dostuff)


  MAKE            MODEL           COST
 -------------------------------------
  FERRARI         360MODENA       $138225.00
  PORSCHE         911GT2          $179900.00
  MITSUBISHI      ECLIPSE         $21000.00
  TOYOTA          CELICA          $21650.00
  LAMBORGHINI     DIABLOGT        $350000.00
  INNOTECH        MYSTERRO        $100000.00
  MCLAREN         F1              $1070000.00
  AUDI            TT              $30500.00
  VECTOR          M12             $184000.00
  SHELBY          SERIES1         $106975.00

Enter car model to search for: SHELBY


  MAKE            MODEL           COST
 -------------------------------------
  SHELBY          SERIES1         $106975.00


  MAKE            MODEL           COST
 -------------------------------------
  MITSUBISHI      ECLIPSE         $21000.00
  TOYOTA          CELICA          $21650.00
  AUDI            TT              $30500.00
  INNOTECH        MYSTERRO        $100000.00
  SHELBY          SERIES1         $106975.00
  FERRARI         360MODENA       $138225.00
  PORSCHE         911GT2          $179900.00
  VECTOR          M12             $184000.00
  LAMBORGHINI     DIABLOGT        $350000.00
  MCLAREN         F1              $1070000.00

||#
